
from multiprocessing import Process, Queue
import time
import queue
from collections import namedtuple
import itertools
from heapq import heappush, heappop

Event = namedtuple('Event', 'time count args')
_QUIT_AFTER_AWAITING_PENDING_TASKS = -1
_QUIT_WITHOUT_AWAITING_PENDING_TASKS = -2

def _consumer_routine ( shared_queue, taskfun ):
	scheduled_event_queue = []
	exit_now = False
	exit_after_tasks_completion = False
	while not exit_now:
		try:
			if len(scheduled_event_queue) == 0:
				#no events in the scheduled event queue: just wait for a new one from the master
				if exit_after_tasks_completion:
					exit_now = True
				else:
					new_event = shared_queue.get ()
			else:
				#there are some events in the scheduled event queue: wait either until a new event arrives from the master or until it's time to execute the next scheduled event
				first_event_time = scheduled_event_queue[0].time
				timeout = first_event_time - time.time ()
				new_event = shared_queue.get (block=True, timeout=timeout)
				
			#new event arrived, put it in the scheduled event queue and wait again
			if new_event.count == _QUIT_AFTER_AWAITING_PENDING_TASKS:
				exit_after_tasks_completion = True
			elif new_event.count == _QUIT_WITHOUT_AWAITING_PENDING_TASKS:
				exit_now = True
			else:
				heappush(scheduled_event_queue, new_event)
			
			
		except queue.Empty:
			#no new events arrived, time to execute the next scheduled event
			priority, count, args = heappop( scheduled_event_queue )
			taskfun ( *args )
				

class MultiprocessingScheduler:
	
	def __init__ ( this, taskfun ):
		this.shared_queue = Queue ()
		this.counter = itertools.count()
		this.collector_process = Process(target=_consumer_routine, args=(this.shared_queue, taskfun))
		this.collector_process.start()
		
	def schedule ( this, event_time, *args ):
		count = next(this.counter)
		new_event = Event (event_time, count, args)
		this.shared_queue.put_nowait ( new_event )
	
	def quit ( this, await_pending_tasks = True ):
		count = _QUIT_AFTER_AWAITING_PENDING_TASKS if await_pending_tasks else _QUIT_WITHOUT_AWAITING_PENDING_TASKS
		quit_event = Event (time.time(), count, None)
		this.shared_queue.put_nowait ( quit_event ) 
		this.collector_process.join()
