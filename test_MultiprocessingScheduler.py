
import time

from MultiprocessingScheduler import MultiprocessingScheduler

def print_a ( a ):
	print ("[print_time][{}] a={}.".format(time.time(),a))

if __name__ == "__main__":
	#unit tests
	mps = MultiprocessingScheduler ( print_a )
	
	mps.schedule ( time.time()+5, 1 )
	print ("[main][{}] scheduled 1 at 5 seconds from now".format(time.time()) )
	mps.schedule ( time.time()+10, 2 )
	print ("[main][{}] scheduled 2 at 10 seconds from now".format(time.time()) )
	mps.schedule ( time.time()+15, 3 )
	print ("[main][{}] scheduled 3 at 15 seconds from now".format(time.time()) )
	
	mps.schedule ( time.time()+7, 4 )
	print ("[main][{}] scheduled 4 at 7 seconds from now (it should appear between 1 and 2)".format(time.time()) )
	
	print ("[main][{}] sleeping 11 seconds".format(time.time()))
	time.sleep(11)
	print ("[main][{}] Here I am again!".format(time.time()) )

	mps.schedule ( time.time()+2, 5 )
	print ("[main][{}] scheduled 5 at 2 seconds from now. It should be the very next event, even before 3!".format(time.time()))
	
	
	print ("[main][{}] Stopping the scheduler after awaiting pending tasks to terminate.".format(time.time()) )
	mps.quit ()
	
	print ("[main][{}] Goodbye.".format(time.time()) )
	
	
	
